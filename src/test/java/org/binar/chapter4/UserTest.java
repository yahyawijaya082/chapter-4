package org.binar.chapter4;

import org.binar.chapter4.controller.UsersController;
import org.binar.chapter4.model.Toko;
import org.binar.chapter4.model.Users;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@SpringBootTest
public class UserTest {

    @Autowired
    private UsersController usersController;

    @Test
    @DisplayName("Test ADD User")
    public void addUser() {
        String response = usersController.addUser("Rizky", "rizky123",
                "rizky@email.com", "VIP");
        Assertions.assertEquals("Add User Success!", response);
    }

    @Test
    @DisplayName("Test GET User")
    public void getUser() {
        Users user = new Users();
        user.setUserId(2);
        user.setUsername("Dika");
        user.setEmail("dika@email.com");
        Assertions.assertEquals("Dika", usersController.getUser(2).getUsername());
    }

    @Test
    @DisplayName("Test GET User Type")
    public void getUserType() {
        usersController.getUserType("VIP");
    }

    @Test
    public void getUserByUsernameAndEmail() {
        usersController.getUserByUsernameAndEmail("Rizky", "rizky@email.com");
    }

    @Test
    public void updateEmail() {
        usersController.updateUser("Rizky", "berbinarbinar@email.com");
    }

    @Test
    public void getUsername() {
        usersController.getUsersByUsername("Dika");
    }

    @Test
    public void getUserHibernate() {
        usersController.getUserHibernate("Dika");
    }

    @Test
    public void getToko() {
        Toko tokoBaru = new Toko(1, "Baygon", 100, 10000);
        Toko toko = usersController.getToko(1);
//        Assertions.
        Assertions.assertEquals(tokoBaru, toko);
    }
}
