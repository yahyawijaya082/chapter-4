package org.binar.chapter4.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Value("${pass.name.rizky}")
    private String username;

    @Value("${password}")
    private String password;

    @Bean
    public String coba() {
        System.out.println("Ini method dijalanin");
        System.out.println("username : " + username);
        System.out.println("password : " + password);
        return "method dijalanin nih";
    }
}
