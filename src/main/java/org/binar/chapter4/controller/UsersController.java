package org.binar.chapter4.controller;

import org.binar.chapter4.model.Toko;
import org.binar.chapter4.model.UserType;
import org.binar.chapter4.model.Users;
import org.binar.chapter4.service.TokoService;
import org.binar.chapter4.service.UserTypeService;
import org.binar.chapter4.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.List;

@Controller
public class UsersController {

    @Autowired
    private UsersService userService;

    @Autowired
    private UserTypeService userTypeService;

    @Autowired
    private TokoService tokoService;

    public String addUser(String username, String password, String email, String typeName) {
        userService.saveUsers(username, password, email, typeName);
        return "Add User Success!";
    }

    public String updateUser(String username, String emailAfter) {
        userService.updateEmail(username, emailAfter);
        return "Update Email success";
    }

    public Users getUser(Integer id) {
        Users user = userService.getUser(id).get();
        System.out.println("username : " + user.getUsername() + "\nemail : " + user.getEmail());
        return user;
    }

    public void getUserType(String typeName) {
        UserType userType = userTypeService.findByTypeName(typeName);
        System.out.println("Type id : " + userType.getType_id() + "\nType Name : " + userType.getTypeName());
    }

    public void getUserByUsernameAndEmail(String username, String email) {
        Users user = userService.getUserByUsernameEmail(username, email);
        System.out.println("Username : " + user.getUsername() + "\nEmail : " + user.getEmail());
    }

    public void getUsersByUsername(String username) {
        Users user = userService.getUsersByUsername(username);
        System.out.println("username : " + user.getUsername() + "\nemail : " + user.getEmail()
            + "\ntype name : " + user.getType_id().getTypeName());
    }

    public void getUserHibernate(String username) {
        Users user = userService.getUsers(username);
        System.out.println("username : " + user.getUsername() + "\nemail : " + user.getEmail()
            + "\ntype name : " + user.getType_id().getTypeName());
    }

    public Toko getToko(Integer id) {
        Toko toko = tokoService.getTokoById(id);
        System.out.println("TOKO : " + toko.getProduk() + "\nHarga : " + toko.getHarga());
        return toko;
    }
}
