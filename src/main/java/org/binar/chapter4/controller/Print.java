package org.binar.chapter4.controller;

import org.binar.chapter4.service.PrintHaloService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class Print {

    @Autowired
    PrintHaloService printHaloService;

    public void printHalo() {
        System.out.println(printHaloService.printHalo());
    }
}
