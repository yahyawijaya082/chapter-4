package org.binar.chapter4.repository;

import org.binar.chapter4.model.Toko;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TokoRepository extends JpaRepository<Toko, Integer> {

    @Query("select te from toko_entity te where id = :id")
    public Toko getToko(Integer id);
}
