package org.binar.chapter4.repository;

import org.binar.chapter4.model.UserType;
import org.hibernate.query.NativeQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface UserTypeRepository extends JpaRepository<UserType, Integer> {
    public UserType findByTypeName(String name);

//    @Query(nativeQuery = true, value = "select * from user_type")
//    public UserType findByblabla(String name);
}
