package org.binar.chapter4.repository;

import org.binar.chapter4.model.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UsersRepository extends JpaRepository<Users, Integer> {

    @Query("select u from users u where u.username = :username and u.email = :email")
    public Users findByUsernameAndEmail(@Param("username") String userName, @Param("email") String email);

//    @Query("select u.username, u.email, ut.type_id, ut.type_name from users u join " +
//            "user_type ut on ut.type_id = u.type_id " +
//            "where u.username = :username ")
//    public UsersUserType findUsersAndType(@Param("username") String username);

    public Users findByUsername(String username);
}
