package org.binar.chapter4.service;

import org.binar.chapter4.model.UserType;
import org.springframework.stereotype.Service;

@Service
public interface UserTypeService {
    public void saveUserType(String typeName);
    public UserType findByTypeName(String typeName);
}
