package org.binar.chapter4.service;

import org.binar.chapter4.model.Toko;

public interface TokoService {

    public Toko getTokoById(Integer id);
}
