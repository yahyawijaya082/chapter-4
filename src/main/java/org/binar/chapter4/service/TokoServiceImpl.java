package org.binar.chapter4.service;

import org.binar.chapter4.model.Toko;
import org.binar.chapter4.repository.TokoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TokoServiceImpl implements TokoService{

    @Autowired
    private TokoRepository tokoRepository;

    @Override
    public Toko getTokoById(Integer id) {
        return tokoRepository.getToko(id);
    }
}
