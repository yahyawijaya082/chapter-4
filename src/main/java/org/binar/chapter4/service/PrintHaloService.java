package org.binar.chapter4.service;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;

@Service
public class PrintHaloService {

    public String printHalo() {
        return "Halo Beans";
    }
}
