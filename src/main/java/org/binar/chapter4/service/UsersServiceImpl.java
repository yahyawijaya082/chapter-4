package org.binar.chapter4.service;

import org.binar.chapter4.model.UserType;
import org.binar.chapter4.model.Users;
import org.binar.chapter4.repository.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.sql.*;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

@Service
public class UsersServiceImpl implements UsersService {

    @PersistenceContext
    private EntityManager em;

    @Autowired
    private UsersRepository usersRepository;

    @Autowired
    private UserTypeService userTypeService;

    @Override
    public void saveUsers(String username, String password, String email, String typeName) {
        // insert log action
        // insert log validation
        // insert table users

        Users user = new Users();
        user.setUsername(username);
        user.setPassword(password);
        user.setEmail(email);
        UserType userType = userTypeService.findByTypeName(typeName);
        if(userType != null) {
            user.setType_id(userType);
        } else {
            userTypeService.saveUserType(typeName);
            user.setType_id(userTypeService.findByTypeName(typeName));
        }
        usersRepository.save(user);
    }

    @Override
    public Optional<Users> getUser(Integer userId) {
        return usersRepository.findById(userId);
    }

    @Override
    public Users getUserByUsernameEmail(String username, String email) {
        return usersRepository.findByUsernameAndEmail(username, email);
    }

    @Override
    public void updateEmail(String username, String newEmail) {
        Users users = usersRepository.findByUsernameAndEmail(username, "rizky@email.com");
        users.setEmail(newEmail);
        // Users 
        // user_id : 2; username : Rizky; email : rizky@email.com; password : password; type_id : 1
        usersRepository.save(users);
    }
//
//    @Override
//    public UsersUserType getUserAndType(String username) {
////        return usersRepository.findUsersAndType(username);
//        return null;
//    }

    @Override
    public Users getUsersByUsername(String username) {
        return usersRepository.findByUsername(username);
    }

    // Contoh pake Entity Manager
    @Override
    public Users getUsers(String username) {
//        String query = "from users where username = ?1";
//        Query q = em.createQuery(query);
//        q.setParameter(1, username);

        String query = "select * from users where username = :username";
        Query q = em.createNativeQuery(query);
        q.setParameter("username", username);
        return (Users) q.getSingleResult();
    }

    // Query pake PreparedStatement
    @Override
    public Users getUsersPS(String username) {
        String query = "select * from users where username = ?";
        try(Connection conn = DriverManager.getConnection("localhost:3306", "root", "password");
            PreparedStatement ps = conn.prepareStatement(query);
        ) {
            ps.setString(1, "Dika");
            ResultSet rs = ps.executeQuery();
            while(rs.next()) {
                System.out.println("username : " + rs.getString("username"));
                System.out.println("email : " + rs.getString("email"));
            }
        } catch (SQLException sqe) {

        }
        return null;
    }
}
