package org.binar.chapter4.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@Entity
public class Book {

    // Algoritma & Pemrograman 1 penulis Rizky
/*    private String judulBuku;
    private String edisi;
    private String penulis; */

    @EmbeddedId
    private BookId bookId;

    private String harga;
    private String quantity;

}
