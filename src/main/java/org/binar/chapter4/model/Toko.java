package org.binar.chapter4.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;

@Setter
@Getter
@Entity(name = "toko_entity")
@Table(name = "toko_table")
public class Toko implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    private String produk; // nama_produk
    private Integer stok;
    private Integer harga;

    public Toko() {

    }

    public Toko(Integer id, String produk, Integer stok, Integer harga) {
        this.id = id;
        this.produk = produk;
        this.stok = stok;
        this.harga = harga;
    }
}
